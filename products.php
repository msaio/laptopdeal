<?php
    shell_exec("./create_temp.sh");
    $str = file_get_contents('temp.json');
    $json = json_decode($str, true);
    
    echo '<div class="container"><div class="row" id="myList">';
    for ($i = 0; $i < sizeof($json); $i++){
        $link = $json[$i]['link'];
        $image = $json[$i]['image'];
        $price = $json[$i]['price'];
        $title = $json[$i]['title'];

        echo '<div class="col-md-3">';
        echo '<a href="' .$link. '">';
        echo '<img class="mx-auto d-block" src="'.$image.'" style="width:100%" alt="'.$title.'">';
        echo '<h2>'.$title.'</h2>';
        echo '<p><span>Price: </span><span>'.$price.'</span></p>';
        echo '</a></div>';
    }
    echo '</div></div>';
    $str2 = file_get_contents('hangchinhhieu_sitemap.json');
    $json2 = json_decode($str2, true);
    if(sizeof($json2) == sizeof($json)){
        echo '<script>
        document.getElementById("crawl").className="btn btn-primary active";
        document.getElementById("crawl").disabled=false;
    </script>';
    }
?>