<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

    <style>
        .progress, .alert {
            margin: 15px;
            display: none;
        }
        .alert {
            display: none;
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark sticky-top">
        <header class="form-inline container-fluid">
            <h1 style="font-style: italic; text-shadow: 5px 5px 40px white; color: #90EE90; ">LAPTOPDEAL</h1>
            <input class="form-control mr-sm-2 ml-3" id="myInput" type="text" placeholder="Search.." style="width: 100%;"> 
        </header>
    </nav>


    <script>
    $(document).ready(function(){
        $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myList .col-md-3").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
    </script>
    <br>
    <div class="container">
        <button id="update" type="button" class="btn btn-primary" onclick="loading()" style="display: block;">Update</button>    
        <script>
            function loadDoc() {
                var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        document.getElementById("products").innerHTML = this.responseText;
                    }
                };
                xhttp.open("GET", "products.php", true);
                xhttp.send();
            }
            function loadResult(){
                var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        document.getElementById("result").innerHTML = this.responseText;
                    }
                };
                xhttp.open("GET", "result.php", true);
                xhttp.send();
            }
            function loading(){
                loadDoc();
                loadResult();
            }
            // var id = setInterval(loadDoc, 1000);
        </script>
        <button id="crawl" type="button" class="btn btn-primary mt-1 " onclick="crawl()">Crawl Data From hangchinhhieu.com</button>
        <script>
            function crawl(){
                start();
                var xhttp = new XMLHttpRequest();
                xhttp.open("GET", "crawl.php", true);
                xhttp.send();
                document.getElementById("crawl").className="btn btn-primary disabled";
                document.getElementById("crawl").disabled=true;
            }
        </script>

        <div class="progress">
            <div class="progress-bar active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
        </div>
        <div class="alert alert-success" role="alert">Loading completed!<div id="result"></div></div>
        <script>
            function start(){
                var $progress = $('.progress');
                var $progressBar = $('.progress-bar');
                var $alert = $('.alert');
                var $update = $('#update');
                $update.css('display', 'block');
                setTimeout(function() {
                    $progress.css('display', 'block');
                    $progressBar.css('width', '10%');
                    setTimeout(function() {
                        $progressBar.css('width', '30%');
                        setTimeout(function() {
                            $progressBar.css('width', '100%');
                            setTimeout(function() {
                                $progress.css('display', 'none');
                                $alert.css('display', 'block');
                            }, 500); // WAIT 5 milliseconds
                        }, 2000); // WAIT 2 seconds
                    }, 1000); // WAIT 1 seconds
                }, 1000); // WAIT 1 second
            }
        </script>
    </div>
    
    <div id="products"></div>


    <footer class="page-footer font-small blue">
        <div class="footer-copyright text-center py-3">© 2018 Copyright:
            <a href="#"> 15520183@gm.uit.edu.vn</a>
        </div>
    </footer>

</body>
</html>